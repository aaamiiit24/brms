-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2021 at 04:48 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.3.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brms_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `ID` int(10) NOT NULL,
  `AdminName` varchar(50) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `UserType` varchar(5) NOT NULL,
  `MobileNumber` bigint(10) DEFAULT NULL,
  `Email` varchar(120) DEFAULT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `AdminRegdate` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`ID`, `AdminName`, `UserName`, `UserType`, `MobileNumber`, `Email`, `Password`, `AdminRegdate`) VALUES
(1, 'Admin', 'test', 'Admin', 8979555556, 'adminuser@gmail.com', '098f6bcd4621d373cade4e832627b4f6', '2019-11-29 12:54:53'),
(2, 'Amit', 'amit', 'Staff', 7508086989, 'test@test.com', '0cb1eb413b8f7cee17701a37a1d74dc3', '2021-11-09 05:49:28');

-- --------------------------------------------------------

--
-- Table structure for table `tblperson`
--

CREATE TABLE `tblperson` (
  `PassbookNo` bigint(20) NOT NULL,
  `PNR` bigint(20) NOT NULL,
  `CID` bigint(20) DEFAULT NULL,
  `AccountNo` bigint(20) NOT NULL,
  `AadharNo` bigint(20) NOT NULL,
  `Name` varchar(200) DEFAULT NULL,
  `FName` varchar(225) DEFAULT NULL,
  `MName` varchar(255) DEFAULT NULL,
  `MStatus` varchar(225) DEFAULT NULL,
  `SName` varchar(225) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Gender` varchar(225) DEFAULT NULL,
  `PanStatus` varchar(225) DEFAULT NULL,
  `PanNo` varchar(225) DEFAULT NULL,
  `PanForm` varchar(225) DEFAULT NULL,
  `caste` varchar(225) DEFAULT NULL,
  `MobileNumber` bigint(10) DEFAULT NULL,
  `Address` mediumtext DEFAULT NULL,
  `NName` varchar(225) DEFAULT NULL,
  `NDOB` date DEFAULT NULL,
  `NAddress` varchar(225) DEFAULT NULL,
  `Picture` varchar(200) DEFAULT NULL,
  `Signature` varchar(225) DEFAULT NULL,
  `DebitAllot` varchar(225) DEFAULT NULL,
  `DebitNumber` varchar(16) DEFAULT NULL,
  `PassbookAllot` varchar(225) DEFAULT NULL,
  `AccountOpeningDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblperson`
--

INSERT INTO `tblperson` (`PassbookNo`, `PNR`, `CID`, `AccountNo`, `AadharNo`, `Name`, `FName`, `MName`, `MStatus`, `SName`, `DOB`, `Gender`, `PanStatus`, `PanNo`, `PanForm`, `caste`, `MobileNumber`, `Address`, `NName`, `NDOB`, `NAddress`, `Picture`, `Signature`, `DebitAllot`, `DebitNumber`, `PassbookAllot`, `AccountOpeningDate`) VALUES
(53, 1111, 1111, 56720212000, 111111111, 'ioyh', 'jhjkhjk', 'jhjk', 'Unmarried', '', '6546-05-04', 'Female', 'No', '', 'Form-60', 'SC', 6546546466, 'hgsdh', '', '0000-00-00', '', '5b4817447ad02da2867e78a61f85d4191636213328.jpg', '5b4817447ad02da2867e78a61f85d4191636213328.jpg', 'Yes', '154652896565', 'No', '2004-04-16'),
(55, 9999, 9999, 56720212000, 9999, 'gjhg', 'hgjhg', 'hjgjh', 'Unmarried', '', '5465-12-06', 'Male', 'No', '', 'Form-60', 'SC', 3545565, 'ghhk', '', '0000-00-00', '', '5b4817447ad02da2867e78a61f85d4191636214207.jpg', '5b4817447ad02da2867e78a61f85d4191636214207.jpg', 'No', '', 'No', '2005-05-04'),
(56, 8888, 88888, 8888, 8888, 'MHJKHJK', 'JKHJKHJKH', 'JHJKH', 'Unmarried', '', '5465-04-06', 'Male', 'No', '', 'Form-60', 'SC', 45645646, 'JHGGJKHJKJKH', '', '0000-00-00', '', '5b4817447ad02da2867e78a61f85d4191636214353.jpg', '5b4817447ad02da2867e78a61f85d4191636214353.jpg', 'No', '', 'No', '0065-04-04');

-- --------------------------------------------------------

--
-- Table structure for table `tblstaff`
--

CREATE TABLE `tblstaff` (
  `ID` int(10) NOT NULL,
  `StaffName` varchar(50) DEFAULT NULL,
  `UserName` varchar(50) DEFAULT NULL,
  `MobileNumber` bigint(10) DEFAULT NULL,
  `Email` varchar(120) DEFAULT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `AdminRegdate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblstaff`
--

INSERT INTO `tblstaff` (`ID`, `StaffName`, `UserName`, `MobileNumber`, `Email`, `Password`, `AdminRegdate`) VALUES
(1, 'Amit', 'amit', 7508086989, 'test@test.com', '098f6bcd4621d373cade4e832627b4f6', '2021-11-06 05:33:58'),
(2, 'kumar', 'kumar', 1111111111, 'test@test.com', '098f6bcd4621d373cade4e832627b4f6', '2021-11-06 10:45:06');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblperson`
--
ALTER TABLE `tblperson`
  ADD PRIMARY KEY (`PassbookNo`);

--
-- Indexes for table `tblstaff`
--
ALTER TABLE `tblstaff`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbladmin`
--
ALTER TABLE `tbladmin`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tblperson`
--
ALTER TABLE `tblperson`
  MODIFY `PassbookNo` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `tblstaff`
--
ALTER TABLE `tblstaff`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
