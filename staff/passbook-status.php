<?php
session_start();
error_reporting(0);
include('../includes/dbconnection.php');
if (strlen($_SESSION['brmsaid']==0)) {
  header('location:logout.php');
  } else{
    if(isset($_POST['submit']))
  {
$eid=$_GET['editid'];
$PassbookStatus=$_POST['PassbookStatus'];
$cidNo=$_POST['cidNo'];


$sql="update tblperson set PassbookAllot='$PassbookStatus', CID='$cidNo' where PassbookNo='$eid'";
                         $query=mysqli_query($conn,$sql);
                         $check=mysqli_fetch_array($query);
                         if(!$check){
                            echo '<script>alert("Person Detail has been updated")</script>';
                         }
  }
  ?>
<!DOCTYPE html>
<html>
<head>
  
  <title>Banking Record Management System | Add Person</title>
    
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('../includes/header.php');?>

 
<?php include_once('../includes/staff-sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Passbook Allotment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="staff-dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Update Passbook Detail</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
              <?php
                   $eid=$_GET['editid'];
                  $sql="SELECT * from tblperson where PassbookNo=$eid";
                  $query = $dbh -> prepare($sql);
                  $query->execute();  
                  $results=$query->fetchAll(PDO::FETCH_OBJ);


                if($query->rowCount() > 0)
                   { 
                      foreach($results as $row)
                        
                        {               ?>
                <h3 class="card-title">PassBook Number: <?php echo htmlentities($row->PassbookNo);?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype="multipart/form-data">
            
                <div class="card-body">
                  <div class="form-group" >
                    <label for="exampleInputEmail1">Is Passbook alloted?*</label>
                    <select class="form-control" name="PassbookStatus" id="PassbookStatus" required="true">
                        <option value="" disabled selected>Select</option>
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                      </select>
                  </div>
                  <div class="form-group" id="CidDiv">
                    <label for="exampleInputEmail1">Customer ID</label>
                    <input type="text" class="form-control" id="cidNo" name="cidNo" required="true">
                  </div>
                 
                <?php $cnt=$cnt+1;}} ?> 
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="submit">Update Details</button>
                  <a href="list-account.php" class="btn btn-primary">Go Back</a>
                  <a href="view-account-detail.php?editid=<?php echo htmlentities ($row->PassbookNo);?>" class ="btn btn-primary">Verify</a> 
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php include_once('../includes/footer.php');?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="../plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../dist/js/demo.js"></script>


<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>

<script>
  $("#PassbookStatus").change(function() {
  if ($(this).val() == "Yes") {
    $('#CidDiv').show();
    $('#cid').attr('required', '');
    $('#cid').attr('data-error', 'This field is required.');
  } else {
    $('#CidDiv').hide();
    $('#cid').removeAttr('required');
    $('#cid').removeAttr('data-error');
  }
});
$("#PassbookStatus").trigger("change");
</script>

</body>
</html>
<?php }  ?>