<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['brmsaid']==0)) {
  header('location:logout.php');
  } else{
    if(isset($_POST['submit']))
  {
$eid=$_GET['editid'];
$DebitAllot=$_POST['DebitAllot'];
$debitNo=$_POST['debitNo'];


$sql="update tblperson set DebitAllot='$DebitAllot', DebitNumber='$debitNo' where PassbookNo='$eid'";
                         $query=mysqli_query($conn,$sql);
                         $check=mysqli_fetch_array($query);
                         if(!$check){
                            echo '<script>alert("Debit Card Detail has been updated.")</script>';
                         }
  }
  ?>
<!DOCTYPE html>
<html>
<head>
  
  <title>BRMS | Banking Resource Management System</title>
    
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once('includes/header.php');?>

 
<?php include_once('includes/sidebar.php');?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Debit Card Allotment</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="dashboard.php">Home</a></li>
              <li class="breadcrumb-item active">Update Debit Card Detail</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
              <?php
                   $eid=$_GET['editid'];
                  $sql="SELECT * from tblperson where PassbookNo=$eid";
                  $query = $dbh -> prepare($sql);
                  $query->execute();  
                  $results=$query->fetchAll(PDO::FETCH_OBJ);


                if($query->rowCount() > 0)
                   { 
                      foreach($results as $row)
                        
                        {               ?>
                <h3 class="card-title">PassBook Number: <?php echo htmlentities($row->PassbookNo);?></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" method="post" enctype="multipart/form-data">
            
                <div class="card-body">
                  <div class="form-group" >
                    <label for="exampleInputEmail1">Is Debit card alloted?*</label>
                    <select class="form-control" name="DebitAllot" id="DebitAllot" required="true">
                        <option> <?php echo htmlentities($row->DebitAllot);?></option>
                        <option value="No">No</option>
                        <option value="Yes">Yes</option>
                      </select>
                  </div>
                  <div class="form-group" id="DebitDiv">
                    <label for="exampleInputEmail1">Debit Card No.</label>
                    <input type="text" class="form-control" id="debitNo" value="<?php echo htmlentities($row->DebitNumber);?>" name="debitNo">
                  </div>
                 
                <?php $cnt=$cnt+1;}} ?> 
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="submit">Update Details</button>
                  <a href="list-account.php" class="btn btn-primary">Go Back</a>
                  <a href="view-account-detail.php?editid=<?php echo htmlentities ($row->PassbookNo);?>" class ="btn btn-primary">Verify</a> 
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 
<?php include_once('includes/footer.php');?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>


<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>

<script>
  $("#DebitAllot").change(function() {
  if ($(this).val() == "Yes") {
    $('#DebitDiv').show();
    $('#debitNo').attr('data-error', 'This field is required.');
  } else {
    $('#DebitDiv').hide();
    $('#debitNo').removeAttr('data-error');
  }
});
$("#DebitAllot").trigger("change");
</script>

</body>
</html>
<?php }  ?>